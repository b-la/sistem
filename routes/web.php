<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Ruta inicial
Route::get('/', function () {
    return view('plantilla.index');
});

//Ruta de los productos
Route::get('/shop',function ()
{
    return view('tienda.productos');
});

//Ruta del hacerca de
Route::get('/about',function ()
{
    return view('about.about');
});

//ruta del contacto
Route::get('/contact',function ()
{
    return view('about.contact');
});

//Ruta del carrito
Route::get('/cart',function ()
{
    return view('carrito.carrito');
});

//ruta del momento de pagar
Route::get('/checkout',function ()
{
    return view('carrito.comprar');
});

//ruta despues de pagar
Route::get('/succes',function ()
{
    return view('carrito.succes');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Ruta para poder ver el perfil
Route::get('/my-account',function ()
{
    return view('usuario.usuario');
});



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
