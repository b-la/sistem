@extends('layouts.app')


@section('content')

     <!-- pages-title-start -->
     <div class="pages-title section-padding">
         <div class="container">
             <div class="row">
                 <div class="col-xs-12">
                     <div class="pages-title-text text-center">
                         <h2>Shop</h2>
                         <ul class="text-left">
                             <li><a href="/">Home </a></li>
                             <li><span> // </span>Shop</li>
                         </ul>
                     </div>
                 </div>
             </div>
         </div>
     </div>
		<!-- pages-title-end -->
		<!-- My account content section start -->
		<section class="pages my-account-page section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="padding60">
							<div class="log-title">
								<h3><strong>My Account</strong></h3>
							</div>

						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="my-right-side">
							<a href="wishlist.html">My wishlists</a>
							<a href="cart.html">Order history and details</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- my account content section end -->

@endsection
