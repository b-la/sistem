@extends('layouts.app')

@section('content')

<div class="pages-title section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="pages-title-text text-center">
                    <h2>Register</h2>
                    <ul class="text-left">
                        <li><a href="index.html">Home </a></li>
                        <li><span> // </span>Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="pages login-page section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="main-input padding60">
                    <div class="log-title">
                        <h3><strong>{{ __('Login') }}</strong></h3>
                    </div>
                    <div class="login-text">
                        <div class="custom-input">
                            <p>Si tienes una cuenta porfavor ingresa</p>
                            <form action="{{ route('login') }}" method="post" >
                                @csrf
                                <div>
                                <input type="email" id="email" name="email"  value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email" />
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div>
                                <input type="password" id="password" name="password" placeholder="Password" />
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div class="submit-text">
                                    <button type="submit">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="main-input padding60 new-customer">
                    <div class="log-title">
                        <h3><strong>registro de usuarios</strong></h3>
                    </div>
                    <div class="custom-input">
                        <form action="{{ route('register') }}" method="post">
                            @csrf
                            <div>
                            <input type="text"  class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Name here.." value="{{ old('name') }}" required autocomplete="name"/>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            </div>
                            <div>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email"/>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                            </div>
                            <div>
                            <input  type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" />
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            </div>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                            
                            <div class="submit-text coupon">
                                <button type="submit">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
