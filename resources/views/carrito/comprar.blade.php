@extends('layouts.app')

@section('content')

     <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="pages-title-text text-center">
							<h2>Checkout</h2>
							<ul class="text-left">
								<li><a href="index.html">Home </a></li>
								<li><span> // </span>Checkout</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
		<!-- Checkout content section start -->
		<section class="pages checkout section-padding">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="main-input single-cart-form padding60">
							<div class="log-title">
								<h3><strong>billing details</strong></h3>
							</div>
							<div class="custom-input">
								<form action="mail.php" method="post">
									<input type="text" name="subject" placeholder="Ciudad" />
									<input type="text" name="subject" placeholder="Telefono" />
									<input type="text" name="subject" placeholder="Departamento" />
									<input type="text" name="subject" placeholder="ZIP" />
									<div class="custom-mess">
										<textarea rows="2" placeholder="Su direccion" name="message"></textarea>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="padding60">
							<div class="log-title">
								<h3><strong>Our order</strong></h3>
							</div>
							<div class="cart-form-text pay-details table-responsive">
								<table>
									<thead>
										<tr>
											<th>Product</th>
											<td>Total</td>
										</tr>
									</thead>
									<tbody>
                                       <tr>
											<th>Men’s White Shirt  x 2</th>
											<td>$86.00</td>
										</tr>
										<tr>
											<th>Men’s Black Shirt  x 1</th>
											<td>$69.00</td>
										</tr>
										<tr>
											<th>Cart Subtotal</th>
											<td>$155.00</td>
										</tr>
										<tr>
											<th>Shipping and Handing</th>
											<td>$15.00</td>
										</tr>
										<tr>
											<th>Vat</th>
											<td>$00.00</td>
                                        </tr>
									</tbody>
									<tfoot>
										<tr>
											<th>Order total</th>
											<td>$325.00</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12 col-sm-6">

					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="padding60">
							<div class="log-title">
								<h3><strong>Payment method</strong></h3>
							</div>
							<div class="categories">
								<ul id="accordion" class="panel-group clearfix">
									<li class="panel">
										<div data-toggle="collapse" data-parent="#accordion" data-target="#collapse1">
											<div class="medium-a">
												direct bank transfer
											</div>
										</div>
										<div class="panel-collapse collapse in" id="collapse1">
											<div class="normal-a">
												<p>Lorem Ipsum is simply in dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
											</div>
										</div>
									</li>
									<li class="panel">
										<div data-toggle="collapse" data-parent="#accordion" data-target="#collapse2">
											<div class="medium-a">
												cheque payment
											</div>
										</div>
										<div class="paypal-dsc panel-collapse collapse" id="collapse2">
											<div class="normal-a">
												<p>Lorem Ipsum is simply in dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
											</div>
										</div>
									</li>
									<li class="panel">
										<div data-toggle="collapse" data-parent="#accordion" data-target="#collapse3">
											<div class="medium-a">
												paypal
											</div>
										</div>
										<div class="paypal-dsc panel-collapse collapse" id="collapse3">
											<div class="normal-a">
												<p>Lorem Ipsum is simply in dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
											</div>
										</div>
									</li>
								</ul>
								<div class="submit-text">
									<a href="/succes">place order</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

@endsection
